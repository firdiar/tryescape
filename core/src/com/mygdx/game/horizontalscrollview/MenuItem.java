package com.mygdx.game.horizontalscrollview;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygdx.game.TryEscape;
import com.mygdx.game.object.LevelData;


public class MenuItem extends Actor {

    private static int N = 0;
    private int currentN;

    private Texture texMenuItem;
    private TextureRegion tex;

    private BitmapFont font;

    private boolean isSelected;

    private LevelData data;

    private float textWidth;

    public MenuItem( float _width, float _height , LevelData data ) {
        texMenuItem = new Texture(Gdx.files.internal("assets/item.png"));
        tex = new TextureRegion(texMenuItem, 0, 0, 128, 128);

        isSelected = false;

        this.data = data;

        this.setWidth(_width);
        this.setHeight(_height);

        font = TryEscape.DEFAULTSKIN.getFont("font-big");
        font.getData().setScale(0.3f);



        N += 1;
        currentN = N;

        GlyphLayout layout = new GlyphLayout(font, String.valueOf(currentN));
        textWidth = layout.width;

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        batch.draw(tex, this.getX(), this.getY(), this.getOriginX(), this.getOriginY(), this.getWidth(), this.getHeight(), this.getScaleX(), this.getScaleY(), this.getRotation());
        if(data.isUnlocked()) {
            if (isSelected()) {
                font.setColor(1, 0, 0, 1);
            } else {
                font.setColor(1, 1, 1, 1);
            }
        }else{
            font.setColor(0, 0, 0, 1);
        }

        font.draw(batch, String.valueOf(currentN), this.getX() + this.getWidth()/2 - (textWidth/2) , this.getY() + this.getHeight()/2 );
    }

    @Override
    public Actor hit(float x, float y, boolean touchable) {
        return x > 0 && x < this.getWidth() && y > 0 && y < this.getHeight()?this:null;
    }


    public boolean touchDown(float x, float y, int pointer) {
        this.setWidth(this.getWidth()+10);
        this.setHeight(this.getHeight() + 10);
        this.setX(this.getX() - 5);
        return true;
    }


    public void touchUp(float x, float y, int pointer) {
        this.setWidth(this.getWidth()-10);
        this.setHeight(this.getHeight()-10);
        this.setX(this.getX() + 5);
    }

    public boolean isSelected(){
        return isSelected;
    }
    public void setSelected(boolean val){
        isSelected = val;
    }

    public LevelData getData(){
        return data;
    }

    public void dispose(){
        texMenuItem.dispose();
        tex.getTexture().dispose();
        font.dispose();
        data = null;

    }
}
