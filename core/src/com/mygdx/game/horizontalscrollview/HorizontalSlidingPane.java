package com.mygdx.game.horizontalscrollview;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.mygdx.game.TryEscape;


public class HorizontalSlidingPane extends Group {

    private float sectionWidth, sectionHeight;

    private Group sections;

    private float amountX = 0;


    private int transmission   = 0;
    private float stopSection  = 0;
    private float speed        = 1500;
    private int currentSection = 1;

    private float flingSpeed   = 1000;

    private float overscrollDistance = 500;

    private Rectangle cullingArea = new Rectangle();
    private Actor touchFocusedChild;
    private ActorGestureListener actorGestureListener;

    private MenuItem selectedItem;

    public HorizontalSlidingPane() {

        sections = new Group();

        this.addActor( sections );



        sectionWidth  = Gdx.app.getGraphics().getWidth();
        sectionHeight = Gdx.app.getGraphics().getHeight();

        actorGestureListener = new ActorGestureListener() {

            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if ( event.getTarget().getClass() == MenuItem.class ) {

                    if(selectedItem != null){
                        selectedItem.setSelected(false);
                        selectedItem = null;
                    }

                    selectedItem = (MenuItem)event.getTarget();
                    selectedItem.setSelected(true);
                }
            }

            @Override
            public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {

                if ( amountX < -overscrollDistance ) return;
                if ( amountX > (sections.getChildren().size - 1) * sectionWidth + overscrollDistance) return;

                amountX -= deltaX;
                //System.out.println(amountX);


                //cancelTouchFocusedChild();
            }

            @Override
            public void fling (InputEvent event, float velocityX, float velocityY, int button) {

                if ( Math.abs(velocityX) > flingSpeed ) {

                    if ( velocityX > 0 ) setStopSection(currentSection - 2);
                    else setStopSection(currentSection);

                }
                if(selectedItem != null){
                    selectedItem.setSelected(false);
                    selectedItem = null;
                }

                cancelTouchFocusedChild();
            }

            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button) {

                if ( event.getTarget().getClass() == MenuItem.class ) {
                    touchFocusedChild = event.getTarget();
                    ((MenuItem)event.getTarget()).touchDown(x ,y ,pointer);
                }

            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                if ( event.getTarget().getClass() == MenuItem.class ) {
                    touchFocusedChild = event.getTarget();
                    ((MenuItem)event.getTarget()).touchUp(x ,y ,pointer);
                }
            }


        };

        this.addListener(actorGestureListener);

    }

    public void addWidget(Actor widget) {
        widget.setX( this.sections.getChildren().size * sectionWidth );
        widget.setY( 25 );
        widget.setWidth( TryEscape.V_WIDTH );
        widget.setHeight( TryEscape.V_HEIGHT );



        sections.addActor( widget );

    }


    public int calculateCurrentSection() {

        int section = Math.round( amountX / sectionWidth ) + 1;

        if ( section > sections.getChildren().size )
            return sections.getChildren().size;
        if ( section < 1 ) return 1;
            return section;
    }

    public int getSectionsCount() {
        return sections.getChildren().size;
    }

    public void setStopSection(int stoplineSection) {

        if ( stoplineSection < 0 ) stoplineSection = 0;
        if ( stoplineSection > this.getSectionsCount() - 1 ) stoplineSection = this.getSectionsCount() - 1;

        stopSection = stoplineSection * sectionWidth;

        if ( amountX < stopSection) {
            transmission = 1;
        }
        else {
            transmission = -1;
        }
    }

    private void move(float delta) {

        if ( amountX < stopSection) {

            if ( transmission == -1 ) {
                amountX = stopSection;

                currentSection = calculateCurrentSection();
                return;
            }

            amountX += speed * delta;

        }
        else if( amountX > stopSection) {
            if ( transmission == 1 ) {
                amountX = stopSection;
                currentSection = calculateCurrentSection();
                return;
            }
            amountX -= speed * delta;
        }
    }

    @Override
    public void act (float delta) {

        // Смещаем контейнер с секциями
        sections.setX( -amountX );

        cullingArea.set( -sections.getX() + 50, sections.getY(), sectionWidth - 100, sectionHeight );
        sections.setCullingArea(cullingArea);


        if ( actorGestureListener.getGestureDetector().isPanning() ) {

            setStopSection(calculateCurrentSection() - 1);
        }
        else {

            move( delta );
        }
    }

    void cancelTouchFocusedChild () {

        if (touchFocusedChild == null) return;

        try {
            this.getStage().cancelTouchFocusExcept(actorGestureListener , touchFocusedChild);
        } catch (Exception e) {

        }

        touchFocusedChild = null;
    }

    public void setFlingSpeed( float _flingSpeed ) {
        flingSpeed = _flingSpeed;
    }

    public void setSpeed( float _speed ) {
        speed = _speed;
    }

    public void setOverscrollDistance( float _overscrollDistance ) {
        overscrollDistance = _overscrollDistance;
    }

    public Group getSections(){
        return sections;
    }

    public boolean isSelecting(){
        return selectedItem!=null;
    }

    public MenuItem getCurrentItem(){
        return selectedItem;
    }

    public void dispose(){
        for(Actor t : sections.getChildren().items){
            if(t==null)
                continue;

            for (Actor a : ((Table)t).getChildren().items){
                if(a != null){
                    ((MenuItem)a).dispose();
                }

            }
        }
    }

}
