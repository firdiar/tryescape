package com.mygdx.game.support;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mygdx.game.TryEscape;

public class ObjectCreatorHelper {

    public static TextField CreateTextField(float scale , float width , String firstText , float x , float y , boolean isScure){
        TextField.TextFieldStyle style = new TextField.TextFieldStyle();
        style.font = TryEscape.DEFAULTSKIN.getFont("font");
        style.cursor = TryEscape.DEFAULTSKIN.getDrawable("black");
        style.font.getData().setScale(scale);
        style.fontColor = isScure ?  Color.WHITE : Color.BLACK;
        style.background = TryEscape.DEFAULTSKIN.getDrawable("textfield");
        style.background.setMinHeight(0);



        TextField textField = new TextField("", style );
        textField.setText(firstText);
        textField.setWidth(width);
        textField.setPosition(x , y - textField.getHeight()/2);

        return textField;

    }

    public static ImageButton CreateButton(String path , EventListener listener){
        Drawable drawable = new TextureRegionDrawable(new TextureRegion( new Texture(path)));
        ImageButton playButton = new ImageButton(drawable);

        playButton.setPosition(300, 100);
        playButton.addListener(listener);
        return  playButton;
    }

    public static Label CreateLabel(String text , float x , float y , float scale){
        Label l = new Label( text ,TryEscape.DEFAULTSKIN );
        l.setFontScale(scale);
        l.setPosition(x,y - l.getHeight()/2);
        return l;
    }

}
