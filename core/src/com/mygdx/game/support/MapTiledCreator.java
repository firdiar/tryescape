package com.mygdx.game.support;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.TryEscape;


public class MapTiledCreator {



    public static Shape getShapeFromObject(MapObject object){
        if (object instanceof RectangleMapObject) {
            return MapTiledCreator.getRectangle((RectangleMapObject)object);
        }
        else if (object instanceof PolygonMapObject) {
            return MapTiledCreator.getPolygon((PolygonMapObject)object);
        }
        else if (object instanceof PolylineMapObject) {
            return MapTiledCreator.getPolyline((PolylineMapObject)object);
        }
        else if (object instanceof CircleMapObject) {
            return MapTiledCreator.getCircle((CircleMapObject)object);
        }
        else {
            return null;
        }
    }

    public static PolygonShape getRectangle(RectangleMapObject rectangleObject) {
        Rectangle rectangle = rectangleObject.getRectangle();
        PolygonShape polygon = new PolygonShape();
        Vector2 size = new Vector2((rectangle.x + rectangle.width * 0.5f) / TryEscape.PPM,
                (rectangle.y + rectangle.height * 0.5f ) / TryEscape.PPM);
        polygon.setAsBox(rectangle.width * 0.5f /TryEscape.PPM,
                rectangle.height * 0.5f /TryEscape.PPM,
                size,
                0.0f);
        return polygon;
    }

    public static CircleShape getCircle(CircleMapObject circleObject) {
        Circle circle = circleObject.getCircle();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(((Circle) circle).radius / TryEscape.PPM);
        circleShape.setPosition(new Vector2(circle.x / TryEscape.PPM, circle.y / TryEscape.PPM));
        return circleShape;
    }

    public static PolygonShape getPolygon(PolygonMapObject polygonObject) {
        PolygonShape polygon = new PolygonShape();
        float[] vertices = polygonObject.getPolygon().getTransformedVertices();

        float[] worldVertices = new float[vertices.length];

        for (int i = 0; i < vertices.length; ++i) {
            System.out.println(vertices[i]);
            worldVertices[i] = vertices[i] / TryEscape.PPM;
        }

        polygon.set(worldVertices);

        return polygon;
    }

    public static ChainShape getPolyline(PolylineMapObject polylineObject) {
        float[] vertices = polylineObject.getPolyline().getTransformedVertices();
        Vector2[] worldVertices = new Vector2[vertices.length / 2];

        for (int i = 0; i < vertices.length / 2; ++i) {
            worldVertices[i] = new Vector2();
            worldVertices[i].x = vertices[i * 2] / TryEscape.PPM;
            worldVertices[i].y = vertices[i * 2 + 1] / TryEscape.PPM;
        }

        ChainShape chain = new ChainShape();
        chain.createChain(worldVertices);
        return chain;
    }

    public static Array<TextureRegion> getTextureRegion(int start , int end , Texture tex , int offestX , int offsetY){
        Array<TextureRegion> frames = new Array<TextureRegion>();
        for(int i= start; i < end; i++) {
            frames.add(new TextureRegion(tex, i * 16 + offestX,  offsetY, 16, 16));
        }
        return frames;
    }

    public static Vector2 getPositionFromObject(MapObject object){
        if (object instanceof RectangleMapObject) {
            return new Vector2(((RectangleMapObject)object).getRectangle().x , ((RectangleMapObject)object).getRectangle().y);
        }
        else if (object instanceof PolygonMapObject) {
            return new Vector2(((PolygonMapObject)object).getPolygon().getX() , ((PolygonMapObject)object).getPolygon().getY());
        }
        else if (object instanceof PolylineMapObject) {
            return new Vector2(((PolylineMapObject)object).getPolyline().getX() , ((PolylineMapObject)object).getPolyline().getY());
        }
        else if (object instanceof CircleMapObject) {
            return new Vector2(((CircleMapObject)object).getCircle().x , ((CircleMapObject)object).getCircle().y);
        }
        else {
            return null;
        }
    }

}
