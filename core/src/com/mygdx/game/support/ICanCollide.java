package com.mygdx.game.support;

public interface ICanCollide {

    public void CollisionEnter(Object col , float cpX , float cpY);
    public void CollisionExit(Object col);
    public void CollisionStay(Object col);
}
