package com.mygdx.game.support;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import javax.swing.*;

public class DirectoryHelper {

    public static String selectDirectory(){
        JFileChooser f = new JFileChooser();
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        f.showSaveDialog(null);
        return f.getSelectedFile().getPath();
    }

    public static void setDirectory(String directory){
        Preferences preferences = Gdx.app.getPreferences("pref");
        preferences.putString( "MapData", directory).flush();
    }

    public static String getDirectory(){
        return Gdx.app.getPreferences("pref").getString("MapData");
    }


}
