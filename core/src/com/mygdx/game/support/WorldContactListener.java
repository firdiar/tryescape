package com.mygdx.game.support;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;


public class WorldContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {

        //System.out.println( contact.getWorldManifold().getPoints()[0].x + "  "+contact.getWorldManifold().getPoints()[0].y);
        if(contact.getFixtureA().getUserData() instanceof ICanCollide ){

            ((ICanCollide)contact.getFixtureA().getUserData()).CollisionEnter(contact.getFixtureB().getUserData(),
                    contact.getWorldManifold().getPoints()[0].x , contact.getWorldManifold().getPoints()[0].y);
        }
        if(contact.getFixtureB().getUserData() instanceof ICanCollide){
            ((ICanCollide)contact.getFixtureB().getUserData()).CollisionEnter(contact.getFixtureA().getUserData(),
                    contact.getWorldManifold().getPoints()[0].x , contact.getWorldManifold().getPoints()[0].y);
        }
    }

    @Override
    public void endContact(Contact contact) {


    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
