package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.game.screen.LoginScreen;
import com.mygdx.game.screen.MainMenuScreen;

public class TryEscape extends Game {
	public static final  int V_WIDTH = 400;
	public static final int V_HEIGHT = 208;
	public static final float PPM = 100f; //pixel per meter
	public static Skin DEFAULTSKIN;
	public SpriteBatch batch;

	
	@Override
	public void create () {
		this.DEFAULTSKIN = new Skin(Gdx.files.internal("skin/glassy-ui.json"));
		batch = new SpriteBatch();
        setScreen(new LoginScreen(this));
	}

	public void ChangeScreen(Screen screen){
		getScreen().dispose();
		setScreen(screen);
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();

	}
}
