package com.mygdx.game.object;

import com.badlogic.gdx.math.Vector2;

public class LevelData {

    public int id;
    private String src;
    private String Level;
    private String fileName;
    private boolean isUnlocked;
    private Vector2 size;
    private Vector2 position;

    public LevelData(int id){
        this.id = id;
    }

    public LevelData(int id , String fileName,boolean isUnlocked){
        this.id = id;
        this.fileName = fileName;
        this.isUnlocked = isUnlocked;
    }

    public boolean isUnlocked(){
        return isUnlocked;
    }

    @Override
    public String toString() {
        return "Currecnt Level : "+id;
    }

    public String getFileName(){
        return  fileName;
    }

}
