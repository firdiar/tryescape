package com.mygdx.game.sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.mygdx.game.TryEscape;
import com.mygdx.game.screen.PlayScreen;
import com.mygdx.game.support.ICanCollide;
import com.mygdx.game.support.MapTiledCreator;

public class MainCharacter extends Sprite implements ICanCollide {
    private World world;
    public Body body;
    private TextureRegion marioStand;
    private TextureRegion marioDie;



    public enum State{JUMPING , STANDING , RUNNING};
    private State prevState;
    private State currState;
    private Animation<TextureRegion> marioRun;
    private Animation<TextureRegion> marioJump;

    private int maxJump = 2;

    private float stateTimer;
    private boolean isRight;

    private boolean isDie = false;
    private boolean isGrounded = true;

    public MainCharacter(PlayScreen screen){
        super(screen.getAtlas().findRegion("little_mario"));
        this.world = screen.getWorld();

        BodyDef bdef = new BodyDef();
        bdef.type = BodyDef.BodyType.DynamicBody;

        bdef.position.set(10/ TryEscape.PPM , 64/TryEscape.PPM);
        body = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6 /TryEscape.PPM);

        currState = State.STANDING;
        prevState = State.STANDING;
        stateTimer = 0;
        isRight = true;

        fdef.shape = shape;

        body.createFixture(fdef).setUserData(this);

        marioRun = new Animation(0.1f , MapTiledCreator.getTextureRegion(1,4 , getTexture(), 1 , 11) );
        marioJump = new Animation(0.1f , MapTiledCreator.getTextureRegion(5 , 6 , getTexture(), 1 , 11) );
        marioStand =  new TextureRegion(getTexture() , 1 , 11 , 16,16);
        marioDie = new TextureRegion(getTexture() , 1+16*6 , 11 , 16,16);
        setBounds(0,0,16/TryEscape.PPM,16/TryEscape.PPM);
        setRegion(marioStand);

    }

    public TextureRegion getFrame(float dt){
        currState = getState();

        TextureRegion reg;

        switch (currState){
            case JUMPING:
                reg = marioJump.getKeyFrame(stateTimer,true);
                break;
            case RUNNING:
                reg = marioRun.getKeyFrame(stateTimer , true);
                break;
            case STANDING:
            default:
                reg = marioStand;
                break;

        }

        if((body.getLinearVelocity().x < 0 || !isRight) && !reg.isFlipX()){
            reg.flip(true , false);
            isRight = false;
        }else if((body.getLinearVelocity().x > 0|| isRight)&& reg.isFlipX()){
            reg.flip(true , false);//ture+true = false;
            isRight = true;
        }
        if(currState == prevState) {
            stateTimer += dt;
        }else{
            prevState = currState;
            stateTimer = 0;
            if(currState != State.JUMPING){

                maxJump = 2;
            }
        }


        return reg;
    }

    public State getState(){
        //System.out.println(body.getLinearVelocity().y);
        if(body.getLinearVelocity().y > 0 || (body.getLinearVelocity().y < 0 && prevState==State.JUMPING)){
            return State.JUMPING;
        }else if((body.getLinearVelocity().x > 0 || body.getLinearVelocity().x < 0) && isGrounded){
            return State.RUNNING;
        }else if(body.getLinearVelocity().x == 0 && body.getLinearVelocity().y== 0 && isGrounded){
            return State.STANDING;
        }else{
            return State.JUMPING;
        }
    }

    public void handleInput(){
        if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE) && maxJump >0){
            maxJump--;
            isGrounded = false;
            System.out.println(maxJump);
            body.setLinearVelocity(body.getLinearVelocity().x,0);
            body.applyLinearImpulse(new Vector2(0,2.4f) , body.getWorldCenter() , true);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT) && body.getLinearVelocity().x <=1f){
            body.applyLinearImpulse (new Vector2(0.2f,0), body.getWorldCenter() , true);
        }else
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)&& body.getLinearVelocity().x >=-1f){
            body.applyLinearImpulse(new Vector2(-0.2f,0) , body.getWorldCenter(),true);
        }else if(!Gdx.input.isKeyPressed(Input.Keys.RIGHT)&&!Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            body.setLinearVelocity(0,body.getLinearVelocity().y);
        }
    }

    public void update(float dt){
        if(!isDie) {
            handleInput();
            setRegion(getFrame(dt));
            setPosition(body.getPosition().x - getWidth()/2 , body.getPosition().y - getHeight()/2);
        }else{
            setPosition(getX() , getY() - (40*dt/TryEscape.PPM));
        }

    }

    @Override
    public void draw(Batch batch) {
        super.draw(batch);
//        batch.begin();
//        batch.draw(getTexture(), 0 , 0);
//        batch.end();
    }

    @Override
    public void CollisionEnter(Object col , float cpX , float cpY) {
        ///System.out.println("Mario Enter Collision");
        //System.out.println(body.getPosition()+"  ");

        if(body != null && col != null && col instanceof Obstacle){

            isDie = true;
            setRegion(marioDie);
            //body.applyLinearImpulse(new Vector2(0,3f) , body.getWorldCenter() , true);
            System.out.println("Masuk");
            setPosition(getX() , getY()+50/TryEscape.PPM);
            System.out.println("Masuk");
            body.setLinearVelocity(0,0);
            body = null;
            System.out.println("Masuk");

        }

        if(body != null && cpY < body.getPosition().y){
            isGrounded = true;
            System.out.println("Grounded");
        }
    }

    @Override
    public void CollisionExit(Object col) {

    }

    @Override
    public void CollisionStay(Object col) {

    }


}
