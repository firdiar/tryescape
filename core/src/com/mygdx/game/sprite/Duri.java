package com.mygdx.game.sprite;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.TryEscape;
import com.mygdx.game.support.MapTiledCreator;

public class Duri extends Obstacle {

    TextureRegion hide;
    private Animation<TextureRegion> show;
    boolean isHitting = false;

    public Duri(World world, BodyDef bdef, FixtureDef fdef , Vector2 pos) {
        super(world, bdef, fdef , new TextureRegion(new Texture("Spike.png")));

        hide = new TextureRegion(getTexture() , 0,0 , 16,16);
        show = new Animation(0.5f , MapTiledCreator.getTextureRegion(1,4 ,getTexture(),1 , 0) );
        setBounds(0,0,16/ TryEscape.PPM,16/TryEscape.PPM);
        setRegion(hide);
        System.out.println("pos : "+pos.x +"  "+ pos.y);
        setPosition(pos.x / TryEscape.PPM, pos.y / TryEscape.PPM);
    }

    @Override
    public void setFrame(float dt) {
        if(isHitting && show.getAnimationDuration() >= 0.3f){
            setRegion(show.getKeyFrame(show.getAnimationDuration() + dt , false));
        }
    }

    @Override
    public void CollisionEnter(Object col, float cpX, float cpY) {
        System.out.println("Spike Collision");
        if(col instanceof MainCharacter){
            isHitting = true;
        }
    }

}
