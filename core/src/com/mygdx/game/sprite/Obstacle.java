package com.mygdx.game.sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.support.ICanCollide;

public abstract class Obstacle extends Sprite implements ICanCollide {

    protected World world;
    protected Body body;

    public Obstacle(World world , BodyDef bdef , FixtureDef fdef , TextureRegion tex){
        super(tex);

        body = world.createBody(bdef);
        body.createFixture(fdef).setUserData(this);
    }

    public abstract void setFrame(float dt);


    @Override
    public void draw(Batch batch) {
        setFrame(Gdx.graphics.getDeltaTime());
        super.draw(batch);
    }



    @Override
    public void CollisionEnter(Object col, float cpX, float cpY) {

    }

    @Override
    public void CollisionExit(Object col) {

    }

    @Override
    public void CollisionStay(Object col) {

    }
}
