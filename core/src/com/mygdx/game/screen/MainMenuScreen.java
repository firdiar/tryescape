package com.mygdx.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.TryEscape;
import com.mygdx.game.horizontalscrollview.HorizontalSlidingPane;
import com.mygdx.game.horizontalscrollview.MenuItem;
import com.mygdx.game.object.LevelData;
import com.mygdx.game.support.DirectoryHelper;
import com.mygdx.game.support.ObjectCreatorHelper;

import javax.swing.*;
import java.io.File;
import java.util.Comparator;

public class MainMenuScreen implements Screen {


    private TryEscape game;

    //Game camera and screen scaling dll
    private OrthographicCamera gamecam;
    private Viewport gamePort;

    //data untuk posisi navi
    private float NAVIPOSX;
    private float NAVIPOSY;
    private float NAVIWIDTH;
    private float NAVIHEIGHT;

    private Texture bg;
    private Stage stage;


    private Texture naviPassive, naviActive;
    private HorizontalSlidingPane slidingMenuPure;

    private Label ldirectory;

    private Array<LevelData> data;


    public MainMenuScreen(final TryEscape game) {
        this.game = game;

        bg = new Texture("overworld_bg.png");
        gamecam = new OrthographicCamera();
        gamePort = new FitViewport(TryEscape.V_WIDTH /TryEscape.PPM,TryEscape.V_HEIGHT /TryEscape.PPM, gamecam );
        gamecam.position.set(gamePort.getWorldWidth()/2  , gamePort.getWorldHeight()/2 , 0);




        data = new Array<LevelData>();
        updateData(data);
        for(LevelData d : data){
            System.out.println(d.id);
        }






        stage = new Stage(new FitViewport(TryEscape.V_WIDTH ,TryEscape.V_HEIGHT , new OrthographicCamera() ) ,  game.batch); //Set up a stage for the ui

        slidingMenuPure = new HorizontalSlidingPane();


        float itemWidth = 640 / 3 - 160;


        int countData = 0;
        int maxSection = (int)Math.ceil(data.size/3.0f);

        for(int section=0; section<maxSection; section++) {
            Table table = new Table();

            for(int i=0; i<1; i++) {
                table.row();
                for(int j = 0; j < 3; j++ ) {
                    if(countData < data.size) {
                        table.add(new MenuItem(itemWidth, itemWidth , data.get(countData))).pad(20);
                        countData++;
                    }else{
                        break;
                    }
                }
            }

            slidingMenuPure.addWidget(table);
        }

        stage.addActor( slidingMenuPure );

        Button btnPlay = ObjectCreatorHelper.CreateButton("playbtn.png" , new EventListener()
        {
            @Override
            public boolean handle(Event event)
            {
                if(event.isHandled()) {
                    System.out.println("Button Play Pressed");
                    LevelData dataLevel;
                    if(slidingMenuPure.getCurrentItem()!=null) {
                        dataLevel = slidingMenuPure.getCurrentItem().getData();
                    }else{
                        System.out.println("No Level Selected");
                        return false;
                    }
                    //mengecek apakah data level ada di direktori
                    File f = new File(DirectoryHelper.getDirectory()+"/tileset_gutter.png");
                    if(!f.exists() && f.isDirectory()) {
                        System.out.println("Image Asset File Not Exist");
                        getFileFromDataBase("tileset_gutter.png");
                        return false;
                    }
                    System.out.println("Image Asset File Exist");

                    f = new File(DirectoryHelper.getDirectory()+"/tileset_gutter_src.tsx");
                    if(!f.exists() && f.isDirectory()) {
                        System.out.println("Src Asset File Not Exist");
                        getFileFromDataBase("tileset_gutter_src.tsx");
                        return false;
                    }
                    System.out.println("Src Asset File Exist");

                    f = new File(DirectoryHelper.getDirectory()+"/"+dataLevel.getFileName());
                    if(!f.exists() && f.isDirectory()) {
                        System.out.println("Map File Data Not Exist");
                        getFileFromDataBase(dataLevel.getFileName());
                        return false;
                    }
                    System.out.println("Map File Data Exist");

                    game.ChangeScreen(new PlayScreen(game , dataLevel ));
                }
                return false;
            }

        });
        ((ImageButton) btnPlay).getImage().setScale(0.7f , 0.6f);
        btnPlay.setPosition((TryEscape.V_WIDTH - (btnPlay.getWidth()*((ImageButton) btnPlay).getImage().getScaleX()))/2, 12 );
        stage.addActor(btnPlay);

        Button btnSelectDir = ObjectCreatorHelper.CreateButton("playbtn.png" , new EventListener()
        {
            @Override
            public boolean handle(Event event)
            {
                if(event.isHandled()) {
                    DirectoryHelper.setDirectory(DirectoryHelper.selectDirectory());
                    System.out.println(DirectoryHelper.getDirectory());
                    ldirectory.setText("Data Dir : "+DirectoryHelper.getDirectory());
                }
                return false;
            }

        });
        ((ImageButton) btnSelectDir).getImage().setScale(0.2f , 0.2f);
        System.out.println(((ImageButton) btnSelectDir).getHeight()*((ImageButton) btnSelectDir).getImage().getScaleY());
        btnSelectDir.setPosition(10,TryEscape.V_HEIGHT-(((ImageButton) btnSelectDir).getHeight()*((ImageButton) btnSelectDir).getImage().getScaleY()) - 10 );
        stage.addActor(btnSelectDir);

        ldirectory = ObjectCreatorHelper.CreateLabel("Data Dir : "+DirectoryHelper.getDirectory(), 35 , btnSelectDir.getY()+5,0.5f);
        stage.addActor(ldirectory);


        naviPassive = new Texture("assets/naviPassive.png");
        naviActive = new Texture("assets/naviActive.png");
        NAVIPOSX = TryEscape.V_WIDTH/(TryEscape.PPM*2) -((slidingMenuPure.getSectionsCount()-1)*10/(TryEscape.PPM*2))  - (naviActive.getWidth()/(TryEscape.PPM));
        NAVIPOSY = 70/TryEscape.PPM;
        NAVIWIDTH = naviActive.getWidth()/(TryEscape.PPM*2);
        NAVIHEIGHT = naviActive.getHeight()/(TryEscape.PPM*2);

        Gdx.input.setInputProcessor(stage);

    }

    private void updateData(Array<LevelData> data){
        data.add(new LevelData(1  , "map1.tmx" , true));
        data.add(new LevelData(3 , "map1.tmx", true));
        data.add(new LevelData(2 , "map1.tmx", true));
        data.add(new LevelData(4, "map1.tmx" , true));
        data.add(new LevelData(6 , "map1.tmx", true));
        data.add(new LevelData(5 , "map1.tmx", true));
        data.add(new LevelData(7, "map1.tmx" , true));
        data.add(new LevelData(8, "map1.tmx" , true));
        data.add(new LevelData(9 , "map1.tmx", true));
        data.add(new LevelData(13));
        data.add(new LevelData(12));
        data.add(new LevelData(11));
        data.add(new LevelData(10));
        data.add(new LevelData(14));
        data.add(new LevelData(15));
        data.add(new LevelData(17));
        data.add(new LevelData(16));
        data.sort(new Comparator<LevelData>() {
            @Override
            public int compare(LevelData o1, LevelData o2) {
                if(o1.id > o2.id){
                    return 1;
                }else if(o1.id == o2.id){
                    return 0;
                }else{
                    return -1;
                }
            }
        });
    }



    public void getFileFromDataBase(String fileName){

    }



    @Override
    public void show() {

    }

    public void Update(){

    }

    @Override
    public void render(float delta) {
        Update();
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.setProjectionMatrix(gamecam.combined);
        game.batch.begin();

        game.batch.draw(bg, 0,0 , bg.getWidth()/TryEscape.PPM , bg.getHeight()/TryEscape.PPM);

        for(int i=1; i<= slidingMenuPure.getSectionsCount(); i++) {
            //(Lebar Layar/2)/ppm + sesi ke N*10/ppm - ((Jumlah sesi-1)*10/2)/ppm - lebar gambar/ppm
            if ( i == slidingMenuPure.calculateCurrentSection() ) {
                game.batch.draw( naviActive,  NAVIPOSX + (i*10/TryEscape.PPM), NAVIPOSY , NAVIWIDTH , NAVIHEIGHT);
            }else {
                game.batch.draw( naviPassive,  NAVIPOSX + (i*10/TryEscape.PPM), NAVIPOSY+0.01f , NAVIWIDTH-0.02f, NAVIHEIGHT-0.02f);
            }
        }

        game.batch.end();

        game.batch.setProjectionMatrix(stage.getCamera().combined) ;

        stage.act(Gdx.graphics.getDeltaTime()); //Perform ui logic
        stage.draw();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        bg.dispose();
        slidingMenuPure.dispose();
        stage.dispose();

    }

    @Override
    public void resize(int width, int height) {

        gamePort.update(width, height);
        stage.getViewport().update(width, height);
    }
}
