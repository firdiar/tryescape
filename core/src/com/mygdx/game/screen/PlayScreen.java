package com.mygdx.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.TryEscape;
import com.mygdx.game.object.LevelData;
import com.mygdx.game.sprite.Duri;
import com.mygdx.game.sprite.MainCharacter;
import com.mygdx.game.sprite.Obstacle;
import com.mygdx.game.support.DirectoryHelper;
import com.mygdx.game.support.MapTiledCreator;
import com.mygdx.game.support.WorldContactListener;

public class PlayScreen implements Screen {

    private TryEscape game;

    private TextureAtlas atlas;

    //Game camera and screen scaling dll
    private OrthographicCamera gamecam;
    private Viewport gamePort;

    //For Importing Map From Tiled Apps
    private TmxMapLoader maploader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;

    //Box 2d Variables for collision DLL
    private World world;
    private Box2DDebugRenderer b2dr;

    //for Character Sprite dll
    private TextureRegion frameChara;

    Array<Obstacle> obstacles;

    private MainCharacter player;

    public PlayScreen(TryEscape game , LevelData data){
        this.game = game;
        atlas = new TextureAtlas("Mario_and_Enemies.pack");
        System.out.println(data.toString());

        gamecam = new OrthographicCamera();
        gamePort = new FitViewport(TryEscape.V_WIDTH /TryEscape.PPM,TryEscape.V_HEIGHT /TryEscape.PPM, gamecam );

        maploader = new TmxMapLoader();
        map = maploader.load(DirectoryHelper.getDirectory()+"/"+data.getFileName());
        renderer =  new OrthogonalTiledMapRenderer(map , 1/TryEscape.PPM);
        gamecam.position.set(gamePort.getWorldWidth()/2  , gamePort.getWorldHeight()/2 , 0);


        //setting world physics
        world = new World(new Vector2(0,-10) , true);
        b2dr =  new Box2DDebugRenderer();

        obstacles = new Array<Obstacle>();
        CreateCollider();

        player = new MainCharacter(this);

        world.setContactListener(new WorldContactListener());

    }

    void CreateCollider(){
        BodyDef bodyDef = new BodyDef();
        FixtureDef fdef = new FixtureDef();

        GenerateBox2D(2,bodyDef,fdef , BodyDef.BodyType.StaticBody);

        GenerateBox2DObstacle(3,bodyDef,fdef , BodyDef.BodyType.KinematicBody);
    }


    void GenerateBox2D(int index , BodyDef bodyDef , FixtureDef fdef , BodyDef.BodyType bodytype){


        for(MapObject object : map.getLayers().get(index).getObjects()){

            bodyDef.type = bodytype;


            fdef.shape = MapTiledCreator.getShapeFromObject(object);
            fdef.friction = 0;

            world.createBody(bodyDef).createFixture(fdef);
        }

    }

    void GenerateBox2DObstacle(int index , BodyDef bodyDef , FixtureDef fdef , BodyDef.BodyType bodytype){


        for(MapObject object : map.getLayers().get(index).getObjects()){

            bodyDef.type = bodytype;


            fdef.shape = MapTiledCreator.getShapeFromObject(object);
            fdef.friction = 0;
            fdef.isSensor = true;

            obstacles.add(new Duri(world , bodyDef , fdef , MapTiledCreator.getPositionFromObject(object)));
            //world.createBody(bodyDef).createFixture(fdef);
        }

    }

    public void update(float dt){
        player.update(dt);

        world.step(1/60f, 6, 2);
        if(player.body != null) {
            gamecam.position.set(player.body.getPosition().x, gamecam.position.y, 0);
        }
        gamecam.update();
        renderer.setView(gamecam);
    }

    public World getWorld(){
        return world;
    }

    public TextureAtlas getAtlas(){
        return atlas;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.setProjectionMatrix(gamecam.combined);
        renderer.render();

        b2dr.render(world,gamecam.combined);
        game.batch.begin();
        player.draw(game.batch);
        for(Obstacle o : obstacles){
            o.draw(game.batch);
        }
        //core.batch.draw(new Texture("Spike.png") , 0 , 0);

        game.batch.end();

    }

    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
