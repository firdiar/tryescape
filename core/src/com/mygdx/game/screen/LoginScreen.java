package com.mygdx.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.TryEscape;
import com.mygdx.game.support.ObjectCreatorHelper;

public class LoginScreen implements Screen {

    private TryEscape game;

    //Game camera and screen scaling dll
    private OrthographicCamera gamecam;
    private Viewport gamePort;

    private Texture img;
    private Stage stage;

    private Label statusLabel;

    private TextField username;
    private TextField password;

    private boolean isInProcess;


    public LoginScreen(TryEscape game){
        this.game = game;
        isInProcess = false;
        img = new Texture("overworld_bg.png");
        gamecam = new OrthographicCamera();
        gamePort = new FitViewport(TryEscape.V_WIDTH /TryEscape.PPM,TryEscape.V_HEIGHT /TryEscape.PPM, gamecam );
        gamecam.position.set(gamePort.getWorldWidth()/2  , gamePort.getWorldHeight()/2 , 0);
        System.out.println(gamecam.position.x+"  "+gamecam.position.y);



        stage = new Stage(new FitViewport(TryEscape.V_WIDTH ,TryEscape.V_HEIGHT , new OrthographicCamera() ) ,  game.batch); //Set up a stage for the ui

        statusLabel = ObjectCreatorHelper.CreateLabel("Status : Input Username & Password" , 250 , TryEscape.V_HEIGHT - 10 , 0.45f);


        Button btnLogin = ObjectCreatorHelper.CreateButton("playbtn.png" , new EventListener()
        {
            @Override
            public boolean handle(Event event)
            {
                if(event.isHandled()) {
                    System.out.println("Button Login Pressed");
                    if(!isInProcess){
                        isInProcess = true;
                        statusLabel.setText("Status : On Process Please Wait...");
                        LoginProcess(username.getText().toString() , password.getText().toString());
                    }
                }
                return false;
            }

        });
        ((ImageButton) btnLogin).getImage().setScale(0.7f , 0.6f);

        btnLogin.setPosition(250,80 -(((ImageButton) btnLogin).getHeight()*((ImageButton) btnLogin).getImage().getScaleY()/2) );



        username = ObjectCreatorHelper.CreateTextField(0.7f , 150 , "UserName" , 50 , 150 , false);

        password = ObjectCreatorHelper.CreateTextField(0.7f , 150 , "" , 50 , 80 , true);


        stage.addActor(statusLabel);
        stage.addActor(btnLogin);
        stage.addActor(ObjectCreatorHelper.CreateLabel("Username", 50 , 180,0.8f));
        stage.addActor(ObjectCreatorHelper.CreateLabel("Password", 50 , 110,0.8f));
        stage.addActor(username);
        stage.addActor(password);


        Gdx.input.setInputProcessor(stage); //Start taking input from the ui

    }

    private void LoginProcess(String username , String password){
        game.ChangeScreen(new MainMenuScreen(game));
    }





    public void handleInput(){
        //gamecam.update();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        handleInput();
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.setProjectionMatrix(gamecam.combined);
        game.batch.begin();
        game.batch.draw(img , 0,0 , img.getWidth()/TryEscape.PPM , img.getHeight()/TryEscape.PPM);


        game.batch.end();

        game.batch.setProjectionMatrix(stage.getCamera().combined) ;

        stage.act(Gdx.graphics.getDeltaTime()); //Perform ui logic
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {
        System.out.println(width+"  "+height);

        gamePort.update(width, height);
        stage.getViewport().update(width, height);
//        for(Actor actor : stage.getActors()){
//            actor.setBounds(actor.getX() , actor.getY() , actor.getWidth() , actor.getHeight());
//        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        img.dispose();
        stage.dispose();


    }
}
